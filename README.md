# airflow-google-iap

This extension will authorise users based on the Google IAP JWT headers.

## Usage

* Setup Google IAP (out of scope for this README, referring to https://cloud.google.com/iap/docs/how-to)

* Add `airflow-google-iap` to your `requirements.txt`

* Configure your `airflow_webserver.py` to use this package:

```python
from flask_appbuilder.security.manager import AUTH_REMOTE_USER
from hibase.google_iap import GoogleIAPSecurityManager

SECURITY_MANAGER_CLASS = GoogleIAPSecurityManager
AUTH_TYPE = AUTH_REMOTE_USER
```

## Configuration

In your `airflow.cfg` you can add the following options.
If you want your users to have a different default role (it is not changed after first registration),
uncomment `# iap_initial_role` and set it to an available role.

```ini
[google]
iap_jwt_audience = ''
# iap_initial_role = Viewer
```

Those options can also be set using environment variables:

```
AIRFLOW__GOOGLE__IAP_JWT_AUDIENCE=""
AIRFLOW__GOOGLE__IAP_INITIAL_ROLE="Viewer"
```

## Synopsis

The code works by creating a subclass of the `AirflowSecurityManager`. It replaces `before_request`,
and checks if the `current_user` is anonymous.

If so, it will setup a user from the IAP credentials received from the IAP JWT header.
Those credentials are verified using the Google Cloud SDK and it will only accept requests from
a matching IAP JWT Audience (see Configuration).

If there is no matching user in the Airflow database, a new user will be created, and gets assigned
the configured initial role.

A request to logout will be redirected to the magic path: `/_gcp_iap/clear_login_cookie` that will
actually log the user out of the Google Identity Aware Proxy.

It is assumed that Google IAP will serve **every request** to Airflow.

You can only safely use this extension, if you can make sure, that **every request** is served via
Google IAP. If you have other means of ingress traffic, besides the Identity Aware Proxy, you will
run into problems with this implementation.

