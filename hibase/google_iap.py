# -*- coding: utf-8 -*-
from flask import abort, request, redirect, flash
from flask_login import current_user, login_user, logout_user
from flask_appbuilder import expose
from flask_appbuilder.security.manager import AUTH_REMOTE_USER
from flask_appbuilder.security.views import AuthView
from airflow.configuration import conf
from airflow.utils.log.logging_mixin import LoggingMixin
from google.auth.transport import requests
from google.oauth2 import id_token
try:
    from airflow.www_rbac.security import AirflowSecurityManager, EXISTING_ROLES
except ImportError:
    try:
        from airflow.www.security import AirflowSecurityManager, EXISTING_ROLES
    except ImportError:
        # Airflow not installed, likely we are running setup.py to _install_ things
        class AirflowSecurityManager(object):
            def __init__(self, appbuilder):
                pass
        EXISTING_ROLES = []

log = LoggingMixin().log

def get_config_param(param, fallback=None):
    return conf.get('google', param, fallback=fallback)

def names_from_email(email):
    email_account, _ = email.split('@')
    names = email_account.split('.')
    first_name = names[0].title()
    try:
        last_name = names[1].title()
    except IndexError:
        last_name = ''
    return (first_name, last_name)

class GoogleIAPUserView(AuthView):
    login_template = ''

    @expose('/logout/')
    def logout(self):
        logout_user()
        return redirect('/_gcp_iap/clear_login_cookie')

class GoogleIAPSecurityManager(AirflowSecurityManager):
    authremoteuserview = GoogleIAPUserView

    def __init__(self, appbuilder):
        super().__init__(appbuilder)
        app = self.appbuilder.get_app
        app.config.setdefault('AUTH_TYPE', AUTH_REMOTE_USER)
        self.expected_audience = get_config_param('iap_jwt_audience')
        self.default_role = get_config_param('iap_initial_role', fallback='Viewer')

    def before_request(self):
        if request.path == '/health':
            return super().before_request()
        token = request.headers.get('x-goog-iap-jwt-assertion')
        if not token:
            log.debug('Missing Google IAP JWT header')
            return abort(403)
        log.debug('Verifying Google IAP JWT header')
        try:
            claims = id_token.verify_token(token, requests.Request(),
                audience=self.expected_audience,
                certs_url='https://www.gstatic.com/iap/verify/public_key')
            log.info('Sucessfully verified IAP JWT claims: {}'.format(claims))
        except Exception as e:
            log.debug('Verifying Google IAP JWT header failed')
            log.error(e)
            return abort(403)

        if current_user.is_anonymous:
            log.debug('Current User is anonymous')
            user = self.find_user(username=claims['sub'])
            first_name, last_name = names_from_email(claims['email'])
            if user is None:
                # Create user from IAP JWT
                log.info('Creating airflow user details for %s from IAP JWT', claims['email'])
                user = self.user_model(
                    username=claims['sub'],
                    first_name=first_name,
                    last_name=last_name,
                    email=claims['email'],
                    roles=[self.find_role('Viewer')],
                    active=True
                )
            else:
                # Update user from IAP JWT
                user.first_name = first_name
                user.last_name = last_name
                user.email = claims['email']
                user.active = True

            self.get_session.add(user)
            self.get_session.commit()
            if not login_user(user):
                raise RuntimeError('Error logging user in')

        super().before_request()

