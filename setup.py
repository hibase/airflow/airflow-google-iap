import pathlib
from setuptools import setup

ROOT = pathlib.Path(__file__).parent
README = (ROOT / "README.md").read_text()

# This call to setup() does all the work
setup(
    name="airflow-google-iap",
    version="1.0.0",
    description="Login to Airflow using Google IAP (Identity Aware Proxy)",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/hibase/airflow-google-iap",
    author="Lukas Rieder",
    author_email="lukas@hibase.co",
    license="MIT",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
    ],
    packages=["hibase"],
    install_requires=[],
)

